package mimo.category;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class CategoryTests {

  @Autowired
  private CategoryService categoryService;

  @Ignore
  @Test
  public void createTestCategory() {
    Category category = new Category(123L, 132L, "test_category", "test");
    categoryService.createCategory(category);

  }
}
