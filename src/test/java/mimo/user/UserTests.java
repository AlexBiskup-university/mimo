package mimo.user;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class UserTests {

  @Autowired
  private UserService userService;

  @Ignore
  @Test
  public void createTestUser() {

    User admin = new User(10000L, "admin", "admin", "admin", "admin", true);
    User user = new User(10001L, "user", "user", "user", "user", false);
    userService.saveUser(admin);
    userService.saveUser(user);


  }

}
