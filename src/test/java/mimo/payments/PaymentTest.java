package mimo.payments;

import java.util.Date;
import mimo.payment.Payment;
import mimo.payment.PaymentService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class PaymentTest {

  @Autowired
  private PaymentService paymentService;

  @Ignore
  @Test
  public void createTestPayment() {
    Payment payment = new Payment(1000L, 1L, "incoming", 11.3, new Date(), "Gas For Car");
    paymentService.addPayment(payment);
  }

}


