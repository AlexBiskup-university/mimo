package mimo.account;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class AccountTests {

  @Autowired
  private AccountService accountService;

  @Ignore
  @Test
  public void createNewAccount() {
    Account account = new Account(123L, "TestAcc", "Standard", 123L);
    accountService.createAccount(account);
  }
}
