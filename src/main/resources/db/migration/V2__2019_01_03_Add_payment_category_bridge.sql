CREATE TABLE payment_category (
  id        bigint(20)   NOT NULL AUTO_INCREMENT,
  category_id bigint(20) NOT NULL,
  payment_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT unique_for_payment UNIQUE (category_id,payment_id),
  CONSTRAINT first_fk FOREIGN KEY (category_id) REFERENCES category (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT second_fk FOREIGN KEY (payment_id) REFERENCES payment (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

