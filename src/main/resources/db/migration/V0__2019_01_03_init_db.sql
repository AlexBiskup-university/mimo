CREATE TABLE user (
  id        bigint(20)   NOT NULL AUTO_INCREMENT,
  email     varchar(255) NOT NULL,
  firstname varchar(255) NOT NULL,
  lastname  varchar(255) NOT NULL,
  password  varchar(255) NOT NULL,
  is_admin  TINYINT      NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  CONSTRAINT unique_email UNIQUE (email)
);

CREATE TABLE account (
  id      bigint(20)   NOT NULl AUTO_INCREMENT,
  user_id bigint(20)   NOT NULL,
  name    varchar(255) NOT NULL,
  type    varchar(255) NOT NULL DEFAULT 'standard',
  PRIMARY KEY (id),
  CONSTRAINT account_fk FOREIGN KEY (user_id) REFERENCES user (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE payment (
  id         bigint(20)    NOT NULL AUTO_INCREMENT,
  account_id bigint(20)    NOT NULL,
  type       varchar(255)  NOT NULL,
  amount     double(11, 2) NOT NULL,
  date       date          NOT NULL,
  PRIMARY KEY (id, account_id),
  CONSTRAINT payment_fk FOREIGN KEY (account_id) REFERENCES account (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT payment_type_check CHECK (type in ('ingoing', 'outgoing'))
);


CREATE TABLE category (
  id                 bigint(20)   NOT NULL AUTO_INCREMENT,
  name               varchar(255) NOT NULL UNIQUE,
  description        varchar(255),
  parent_category_id bigint(20),
  PRIMARY KEY (id),
  CONSTRAINT category_fk FOREIGN KEY (parent_category_id) REFERENCES category (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE report (
  id         bigint(20) NOT NULL AUTO_INCREMENT,
  start_date date       NOT NULL,
  end_date   date       NOT NULL,
  ingoings   int(11),
  outgoings  int(11),
  account_id bigint(20),
  PRIMARY KEY (id),
  CONSTRAINT report_fk FOREIGN KEY (account_id) REFERENCES account (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);




