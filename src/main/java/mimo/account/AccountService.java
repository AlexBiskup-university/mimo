package mimo.account;

import java.util.AbstractList;
import java.util.Optional;

public interface AccountService {

  Account createAccount(Account account);

  Iterable<Account> saveAllAccounts(AbstractList<Account> accounts);

  Iterable<Account> getAllAccounts();

  Iterable<Account> getAllUserAccounts(Long userId);

  Optional<Account> getAccountById(Long account_id);

  void deleteAccount(long account_id);

  void updateAccount(Long account_id, String account_name, String account_type);

}
