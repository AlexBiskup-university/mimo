package mimo.account;

import mimo.user.User;
import mimo.user.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping("/accounts")
public class AccountController {

    private AccountService accountService;
    private UserService userService;

    public AccountController(AccountService accountService, UserService userService) {

        this.accountService = accountService;
        this.userService = userService;
    }

    @GetMapping
    public String getAllAccounts(Authentication auth, Model model) {
        User user = userService.getUserFromAuth(auth);
        addPathToModel(model);
        addAccountsToModel(model, user);
        return "accounts/accounts";
    }

    @PostMapping
    public String createAccount(Authentication auth, @RequestParam("account_name") String name, @RequestParam("account_type")
            String type, Model model) {
        User user = userService.getUserFromAuth(auth);
        addPathToModel(model);
        Account newAccount = new Account(null, name, type, user.getId());
        accountService.createAccount(newAccount);
        addAccountsToModel(model, user);
        return "accounts/accounts";
    }

    @GetMapping("/add")
    public String getAddAccountPage(Model model) {
        addPathToModel(model);
        return "accounts/addAccount";
    }

    @GetMapping("/{accountId}")
    public String getAccount(@PathVariable("accountId") Long accountId, Model model) {
        addPathToModel(model);
        Account account = null;
        Optional<Account> opAccount = accountService.getAccountById(accountId);
        if (opAccount.isPresent()) {
            account = opAccount.get();
        }

        model.addAttribute("account", account);
        return "accounts/account";
    }


    @PutMapping("/{accountId}")
    public String updateAccount(@PathVariable("accountId") Long account_id, @RequestParam("account_name") String account_name, @RequestParam("account_type") String account_type, Model model) {
        addPathToModel(model);
        accountService.updateAccount(account_id, account_name, account_type);
        Account account = null;
        Optional<Account> opAccount = accountService.getAccountById(account_id);
        if (opAccount.isPresent()) {
            account = opAccount.get();
        }
        model.addAttribute("account", account);

        return "redirect:/accounts";
    }

    @PostMapping("/delete")
    public String deleteAccount(@RequestParam("account_id") Long account_id, @RequestParam("userId") Long userId, Model model) {
        addPathToModel(model);
        accountService.deleteAccount(account_id);

        model.addAttribute("accounts", accountService.getAllUserAccounts(userId));
        return "accounts/accounts";
    }

    @DeleteMapping("/{accountId}")
    public String deleteAccount(Authentication auth, @PathVariable ("accountId") Long account_id, Model model) {
        addPathToModel(model);
        User user = userService.getUserFromAuth(auth);
        accountService.deleteAccount(account_id);

        model.addAttribute("accounts", accountService.getAllUserAccounts(user.getId()));
        return "redirect:/accounts";
    }



    private void addPathToModel(Model model) {
        model.addAttribute("path", "/accounts");
    }

    private void addAccountsToModel(Model model, User user) {
        if (user.isAdmin()) {
            model.addAttribute("accounts", accountService.getAllAccounts());
        } else {
            model.addAttribute("accounts", accountService.getAllUserAccounts(user.getId()));
        }
    }


}
