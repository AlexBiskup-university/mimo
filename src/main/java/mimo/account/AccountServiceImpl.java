package mimo.account;

import java.util.AbstractList;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

  private AccountRepository accountRepository;

  public AccountServiceImpl(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @Override
  public Account createAccount(Account account) {
    return accountRepository.save(account);
  }

  @Override
  public Iterable<Account> saveAllAccounts(AbstractList<Account> accounts) {
    return accountRepository.saveAll(accounts);
  }

  @Override
  public Iterable<Account> getAllAccounts() {
    return accountRepository.findAll();
  }

  @Override
  public Iterable<Account> getAllUserAccounts(Long userId) {
    return accountRepository.findAccountByUserId(userId);
  }

  @Override
  public Optional<Account> getAccountById(Long account_id) {
    return accountRepository.findById(account_id);
  }

  @Override
  public void deleteAccount(long account_id) {
    accountRepository.deleteById(account_id);
  }

  @Override
  public void updateAccount(Long account_id, String account_name, String account_type) {
    Optional<Account> optionalAccount = accountRepository.findById(account_id);
    if (optionalAccount.isPresent()) {
      Account account = optionalAccount.get();
      account.setName(account_name);
      account.setType(account_type);
      accountRepository.save(account);
    }
  }
}
