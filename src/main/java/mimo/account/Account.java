package mimo.account;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import mimo.payment.Payment;

@Entity
public class Account {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Long id;
  private String name;
  private String type;
  private Long userId;
  @Transient
  private Iterable<Payment> payments;

  public Account(Long id, String name, String type, Long userId) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.userId = userId;
  }

  public Account() {
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String account_name) {
    this.name = account_name;
  }

  public String getType() {
    return type;
  }

  public void setType(String account_type) {
    this.type = account_type;
  }

  public long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Iterable<Payment> getPayments() {
    return payments;
  }

  public void setPayments(Iterable<Payment> payments) {
    this.payments = payments;
  }
}
