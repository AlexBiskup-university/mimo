package mimo;

import mimo.data.DataGeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class HomeController {

  @Autowired
  private DataGeneratorService dataGeneratorService;

  @GetMapping
  public String redirectToAccounts() {
    return "redirect:/accounts";
  }


  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/generator")
  public String getGenerateDataPage() {
    return "generator";
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping("/generator")
  public String generateData(@RequestParam("users") Long users,
      @RequestParam("accounts") Long accounts, @RequestParam("payments") Long payments, @RequestParam("categories") Long categories) {
    dataGeneratorService.generateData(users, accounts, payments, categories);
    return "redirect:/";
  }


}
