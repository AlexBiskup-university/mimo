package mimo;

import mimo.user.User;
import mimo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AdminInitializer {

  @Autowired
  UserService userService;

  @EventListener
  public void appReady(ApplicationReadyEvent event) {

    try {
      userService.saveUser(new User(null, "admin", "admin", "admin", "admin", true));
    } catch (Exception e) {
      // TO NOTHING
    }
  }
}
