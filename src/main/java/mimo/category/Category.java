package mimo.category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long parentCategoryId;
    private String name;
    private String description;

    public Category(Long id, Long parentCategoryId, String name, String description) {
        this.id = id;
        this.parentCategoryId = parentCategoryId;
        this.name = name;
        this.description = description;
    }

    public Category(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Long parentCategoryId) {
        this.parentCategoryId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) { this.description = description; }
}
