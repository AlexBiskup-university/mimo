package mimo.category;

import java.util.AbstractList;

public interface CategoryService {

  void createCategory(Category category);

  Iterable<Category> readCategory();

  Category getCategoryById(Long id);

  void updateCategory(Category category);

  void deleteCategory(long id);

  Iterable<Category> getAllCategories();

    Iterable<Category> saveAllCategories(AbstractList<Category> categories);
}
