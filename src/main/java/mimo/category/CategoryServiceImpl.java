package mimo.category;

import java.util.AbstractList;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {

  private CategoryRepository categoryRepository;

  public CategoryServiceImpl(CategoryRepository categoryRepository) {
    this.categoryRepository = categoryRepository;
  }

  @Override
  public void createCategory(Category category) {
    categoryRepository.save(category);
  }

  @Override
  public Iterable<Category> readCategory() {
    return categoryRepository.findAll();
  }

  @Override
  public void updateCategory(Category category) {
    categoryRepository.save(category);
  }

  @Override
  public void deleteCategory(long id) {
    categoryRepository.deleteById(id);
  }

  @Override
  public Iterable<Category> getAllCategories() {
    return categoryRepository.findAll();
  }

  @Override
  public Category getCategoryById(Long id) {
    return categoryRepository.findById(id).get();
  }

  @Override
  public Iterable<Category> saveAllCategories(AbstractList<Category> categories) {
    return categoryRepository.saveAll(categories);
  }
}
