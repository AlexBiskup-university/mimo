package mimo.category;

import mimo.user.User;
import mimo.user.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/categories")

public class CategoryController {

  private CategoryService categoryService;
  private UserService userService;

  public CategoryController(CategoryService categoryService, UserService userService) {
    this.categoryService = categoryService;
    this.userService = userService;
  }

  @GetMapping
  public String getCategories(Authentication auth, Model model) {
    User user = userService.getUserFromAuth(auth);
    model.addAttribute("user", user);
    model.addAttribute("categories", categoryService.readCategory());
    addPathToModel(model);
    return "categories/categories";
  }

  @PostMapping
  public String addCategory(@RequestParam("name") String category_name,
      @RequestParam("description") String category_description, Model model) {
    addPathToModel(model);

    model.addAttribute("category", new Category());

    Category newCategory = new Category(null, null, category_name, category_description);
    categoryService.createCategory(newCategory);

    model.addAttribute("categories", categoryService.readCategory());
    categoryService.createCategory(newCategory);

    return "redirect:/categories";
  }

  @GetMapping("/add")
  public String getAddCategoryPage(Model model) {
    addPathToModel(model);

    addPathToModel(model);
    return "categories/addCategory";
  }

  @GetMapping("/{categoryId}/update")
  public String getUpdateCategoryPage(@PathVariable("categoryId") Long categoryId, Model model) {

    addPathToModel(model);
    model.addAttribute("category", categoryService.getCategoryById(categoryId));
    addPathToModel(model);
    return "categories/update";
  }


  @PutMapping("/{categoryId}")
  public String updateCategory(@PathVariable("categoryId") Long categoryId,
      @RequestParam("name") String categoryName,
      @RequestParam("description") String description) {
    Category category = categoryService.getCategoryById(categoryId);
    category.setDescription(description);
    category.setName(categoryName);
    categoryService.updateCategory(category);
    return "redirect:/categories";
  }

  @PostMapping("/delete")
  public String deleteCategory(@RequestParam("category-id") Long categoryId, Model model) {
    addPathToModel(model);

    categoryService.deleteCategory(categoryId);
    model.addAttribute("categories", categoryService.readCategory());
    return "redirect:/categories";
  }

  public void addPathToModel(Model model) {
    model.addAttribute("path", "/categories");
  }
}
