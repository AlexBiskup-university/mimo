package mimo.rest;

import java.util.ArrayList;
import mimo.account.Account;
import mimo.account.AccountService;
import mimo.category.Category;
import mimo.category.CategoryService;
import mimo.payment.Payment;
import mimo.payment.PaymentService;
import mimo.paymentCategory.PaymentCategoryService;
import mimo.user.User;
import mimo.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ApiController {

  @Autowired
  private UserService userService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private AccountService accountService;

  @Autowired
  private PaymentService paymentService;

  @Autowired
  private PaymentCategoryService paymentCategoryService;

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/users")
  public Iterable<User> getAllUsers() {
    return userService.getAllUsersExceptAdmins();
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/categories")
  public Iterable<Category> getAllCategories() {
    return categoryService.getAllCategories();
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/accounts")
  public Iterable<Account> getAllAccounts() {
    Iterable<Account> accounts = accountService.getAllAccounts();
    for (Account account : accounts) {
      account.setPayments(paymentService.getPaymentsByAccountId(account.getId()));
      for (Payment p : account.getPayments()) {
        ArrayList<Long> categoryIds = new ArrayList<>();
        paymentCategoryService.getCategoriesByPaymentId(p.getId())
            .forEach(category -> categoryIds.add(category.getCategory().getId()));
        p.setCategories(categoryIds);
      }

    }
    return accounts;
  }

}
