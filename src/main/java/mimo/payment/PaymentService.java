package mimo.payment;

import java.text.ParseException;
import java.util.AbstractList;
import java.util.Date;


public interface PaymentService {

    public Payment addPayment(Payment payment);

    public Iterable<Payment> getPaymentsByAccountId(Long account_id);

    public Iterable<Payment> getPayments();

    public Payment getPaymentById(long id);

    public void deletePayment(long id);

    public double getBalance(Long accountId);

    public Date formatDate(String date) throws ParseException;

    Iterable<Payment> saveAllPayments(AbstractList<Payment> payments);

}
