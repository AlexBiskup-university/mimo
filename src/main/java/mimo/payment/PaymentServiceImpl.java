package mimo.payment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.AbstractList;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {


  private PaymentRepository paymentRepository;

  public PaymentServiceImpl(PaymentRepository paymentRepository) {
    this.paymentRepository = paymentRepository;
  }

  @Override
  public Payment addPayment(Payment payment) {
    return paymentRepository.save(payment);
  }

  @Override
  public Iterable<Payment> getPaymentsByAccountId(Long accountId) {
    return paymentRepository.findPaymentByAccountId(accountId);
  }

  @Override
  public Iterable<Payment> getPayments() {
    return paymentRepository.findAll();
  }

  @Override
  public Payment getPaymentById(long id) {
    return paymentRepository.findById(id).get();
  }

  @Override
  public void deletePayment(long id) {
    paymentRepository.deleteById(id);
  }

  @Override
  public double getBalance(Long accountId) {
    Iterable<Payment> payments = getPaymentsByAccountId(accountId);
    double balance = 0;
    for (Payment payment : payments) {
      balance += payment.getAmount();
    }
    return balance;
  }

  @Override
  public Date formatDate(String date) throws ParseException {
    return new SimpleDateFormat("dd-MM-yyyy").parse(date);
  }

  @Override
  public Iterable<Payment> saveAllPayments(AbstractList<Payment> payments) {
    return paymentRepository.saveAll(payments);
  }

}
