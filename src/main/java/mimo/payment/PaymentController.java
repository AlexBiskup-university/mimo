package mimo.payment;

import java.text.ParseException;
import mimo.category.CategoryService;
import mimo.paymentCategory.PaymentCategory;
import mimo.paymentCategory.PaymentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/accounts/{accountId}/payments")
public class PaymentController {

  private PaymentService paymentService;

  @Autowired
  private PaymentCategoryService paymentCategoryService;

  @Autowired
  private CategoryService categoryService;

  public PaymentController(PaymentService paymentService) {
    this.paymentService = paymentService;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
  }

  @GetMapping
  public String getPayments(@PathVariable("accountId") Long accountId, Model model) {
    addPathToModel(model);
    model.addAttribute("payments", paymentService.getPaymentsByAccountId(accountId));
    model.addAttribute("balance", paymentService.getBalance(accountId));
    return "payments/payments";
  }

  @PostMapping
  public String addPayment(@PathVariable("accountId") Long accountId,
      @RequestParam("amount") double amount, @RequestParam("type") String type,
      @RequestParam("date") String date, @RequestParam("name") String name, Model model)
      throws ParseException {
    addPathToModel(model);
    Payment newPayment = new Payment(null, accountId, type, amount, paymentService.formatDate(date),
        name);
    paymentService.addPayment(newPayment);
    return getRedirectLink(accountId);
  }

  @PutMapping("/{paymentId}")
  public String updatePayment(@PathVariable("accountId") Long accountId,
      @PathVariable("paymentId") Long paymentId, @RequestParam("name") String name, @RequestParam("amount") double amount,
      @RequestParam("type") String type, @RequestParam("date") String date, Model model)
      throws ParseException {
    addPathToModel(model);

    Payment paymentToUpdate = paymentService.getPaymentById(paymentId);

    if(!name.equals("") && !name.equals(paymentToUpdate.getName())){
      paymentToUpdate.setName(name);
    }

    if (!date.isEmpty() && date != null) {
      paymentToUpdate.setDate(paymentService.formatDate(date));
    }

    if (amount != paymentToUpdate.getAmount()) {
      paymentToUpdate.setAmount(amount);
    }
    if (!type.equals(paymentToUpdate.getType())) {
      paymentToUpdate.setAmount(-paymentToUpdate.getAmount());
      paymentToUpdate.setType(type);
    }

    paymentService.addPayment(paymentToUpdate);

    return getRedirectLink(accountId);
  }


  @DeleteMapping("/{paymentId}")
  public String deleteUser(@PathVariable("accountId") Long accountId,
      @PathVariable("paymentId") Long paymentId, Model model) {
    addPathToModel(model);

    paymentService.deletePayment(paymentId);
    return getRedirectLink(accountId);
  }

  @GetMapping("/add")
  public String getAddPaymentPage(@PathVariable("accountId") Long accountId, Model model) {
    addPathToModel(model);

    model.addAttribute("accountId", accountId);
    return "payments/add";
  }

  @GetMapping("{paymentId}/update")
  public String getUpdatePaymentPage(@PathVariable("accountId") Long accountId,
      @PathVariable("paymentId") Long paymentId, Model model) {
    addPathToModel(model);

    model.addAttribute("payment", paymentService.getPaymentById(paymentId));
    model.addAttribute("paymentCategories",
        paymentCategoryService.getCategoriesByPaymentId(paymentId));
    return "payments/update";
  }

  @GetMapping("{paymentId}/addCategory")
  public String getAddCategoryPage(@PathVariable("accountId") Long accountId,
      @PathVariable("paymentId") Long paymentId, Model model) {
    addPathToModel(model);

    model.addAttribute("payment", paymentService.getPaymentById(paymentId));
    model.addAttribute("accountId", accountId);
    model.addAttribute("categories", categoryService.readCategory());
    return "payments/addCategory";
  }


  @PostMapping("{paymentId}/addCategory")
  public String addCategory(@PathVariable("accountId") Long accountId,
      @PathVariable("paymentId") Long paymentId, @RequestParam("categoryId") Long categoryId,
      Model model) {
    addPathToModel(model);

    PaymentCategory paymentCategory = new PaymentCategory(
        categoryService.getCategoryById(categoryId), paymentId);

    paymentCategoryService.save(paymentCategory);

    return "redirect:/accounts/" + accountId + "/payments";
  }

  private void addPathToModel(Model model) {
    model.addAttribute("path", "/accounts");
  }

  private String getRedirectLink(Long accountId) {
    return "redirect:/accounts/" + accountId + "/payments";
  }
}
