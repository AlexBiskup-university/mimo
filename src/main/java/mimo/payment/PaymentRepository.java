package mimo.payment;

import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    public Iterable<Payment> findPaymentByAccountId(Long accountId);

}
