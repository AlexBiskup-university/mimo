package mimo.payment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity
public class Payment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private Long accountId;
  private String type;
  private double amount;
  private Date date;
  private String name;
  @Transient
  private ArrayList<Long> categories;

  public Payment(Long id, Long accountId, String type, double amount, Date date, String name) {
    this.id = id;
    this.accountId = accountId;
    this.type = type;
    if (type.equals("Outgoing")) {
      this.amount = -amount;
    } else {
      this.amount = amount;
    }
    this.date = date;
    this.name = name;
    this.categories = new ArrayList<>();
  }

  public Payment() {

  }

  public Long getId() {
    return id;
  }

  public void setId(Long payment_id) {
    this.id = payment_id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }

  public String getDateAsString() throws ParseException {
    return new SimpleDateFormat("dd-MM-yyyy").format(this.date);
  }

  public Date getDate() throws ParseException {
    return this.date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<Long> getCategories() {
    return categories;
  }

  public void setCategories(ArrayList<Long> categories) {
    this.categories = categories;
  }
}
