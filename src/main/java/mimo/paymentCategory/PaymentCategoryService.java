package mimo.paymentCategory;

public interface PaymentCategoryService {

    public Iterable<PaymentCategory> getCategoriesByPaymentId(Long paymentId);

    void save(PaymentCategory paymentCategory);

}
