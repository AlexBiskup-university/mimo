package mimo.paymentCategory;

import org.springframework.data.repository.CrudRepository;

public interface PaymentCategoryRepository extends CrudRepository<PaymentCategory, Long> {

    public Iterable<PaymentCategory> findByPaymentId(Long paymentId);
}
