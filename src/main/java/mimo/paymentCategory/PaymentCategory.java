package mimo.paymentCategory;

import mimo.category.Category;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class PaymentCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @OneToOne
    private Category category;
    private Long paymentId;

    public PaymentCategory(Category category, Long paymentId) {
        this.category = category;
        this.paymentId = paymentId;
    }

    public PaymentCategory() {

    }


    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}


