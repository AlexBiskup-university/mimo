package mimo.paymentCategory;

import org.springframework.stereotype.Service;

@Service
public class PaymentCategoryServiceImpl implements PaymentCategoryService {

    private PaymentCategoryRepository paymentCategoryRepository;

    public PaymentCategoryServiceImpl(PaymentCategoryRepository paymentCategoryRepository) {
        this.paymentCategoryRepository = paymentCategoryRepository;
    }

    public Iterable<PaymentCategory> getCategoriesByPaymentId(Long paymentId) {
        return paymentCategoryRepository.findByPaymentId(paymentId);
    }

    @Override
    public void save(PaymentCategory paymentCategory) {
        paymentCategoryRepository.save(paymentCategory);
    }


}
