package mimo.data;

public interface DataGeneratorService {

    void generateData(Long amountOfUsers, Long amountOfAccountsPerUser, Long amountOfPaymentsPerAccount, Long amountOfCategories);

}
