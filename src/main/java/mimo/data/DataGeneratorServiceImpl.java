package mimo.data;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import mimo.account.Account;
import mimo.account.AccountService;
import mimo.category.Category;
import mimo.category.CategoryService;
import mimo.payment.Payment;
import mimo.payment.PaymentService;
import mimo.user.User;
import mimo.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataGeneratorServiceImpl implements DataGeneratorService {

  char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
  Random random = new Random();

  private Logger log = LoggerFactory.getLogger("DataGenerator");

  @Autowired
  private UserService userService;

  @Autowired
  private AccountService accountService;

  @Autowired
  private PaymentService paymentService;

  @Autowired
  private CategoryService categoryService;

  @Override
  public void generateData(Long amountOfUsers, Long amountOfAccountsPerUser,
      Long amountOfPaymentsPerAccount, Long amountOfCategories) {
    log.info("START DATA GENERATOR");
    Iterable<User> users = generateUsers(amountOfUsers);
    log.info("ALL USERS SAVED");
    Iterable<Account> accounts = generateAccounts(amountOfAccountsPerUser, users);
    log.info("ALL ACCOUNTS SAVED");
    generatePayments(amountOfPaymentsPerAccount, accounts);
    log.info("ALL PAYMENTS SAVED");
    generateCategories(amountOfCategories);
    log.info("ALL CATEGORIES SAVED");
    log.info("DATA GENERATOR FINISHED");
  }

  private Iterable<Category> generateCategories(Long amountOfCategories) {
      AbstractList<Category> categories = new ArrayList<>();
      for (Long i = 0L; i < amountOfCategories; i++) {
          categories.add(generateRandomCategory());
          log.info("CATEGORY: " + i);
      }
      return categoryService.saveAllCategories(categories);
  }

  private Category generateRandomCategory() {
      String name = generateRandomName();
      String description = generateRandomName();
      return new Category(null, null, name, description);
  }

  private void generatePayments(Long amountOfPaymentsPerAccount, Iterable<Account> accounts) {
    AbstractList<Payment> payments = new ArrayList<>();
    for (Account account : accounts) {
      payments.addAll(generatePaymentsForAccount(account.getId(), amountOfPaymentsPerAccount));
    }
    paymentService.saveAllPayments(payments);
  }

  private AbstractList<Payment> generatePaymentsForAccount(Long accountId,
      Long amountOfPaymentsPerAccount) {
    AbstractList<Payment> payments = new ArrayList<>();
    for (Long i = 0L; i < amountOfPaymentsPerAccount; i++) {
      payments.add(generateRandomPayment(accountId));
      log.info("ACCOUNT: " + accountId + ", PAYMENT: " + i);
    }
    return payments;
  }

  private Payment generateRandomPayment(Long accountId) {
    String type = getRandomPaymentType();
    double amount = getRandomAmount();
    String name = generateRandomName();
    return new Payment(null, accountId, type, amount, new Date(), name);
  }


  private double getRandomAmount() {
    return random.nextDouble() + random.nextInt(1000);
  }


  private Iterable<Account> generateAccounts(Long amountOfAccountsPerUser,
      Iterable<User> users) {
    AbstractList<Account> accounts = new ArrayList<>();
    for (User user : users) {
      accounts
          .addAll(createAccountsForUser(user.getId(), amountOfAccountsPerUser));
    }
    return accountService.saveAllAccounts(accounts);
  }


  private AbstractList<Account> createAccountsForUser(Long userId, Long amountOfAccountsPerUser) {
    AbstractList<Account> accounts = new ArrayList<>();
    for (Long i = 0L; i < amountOfAccountsPerUser; i++) {
      accounts.add(generateRandomAccount(userId));
      log.info("USER: " + userId + ", ACCOUNT: " + i);
    }
    return accounts;
  }

  private Account generateRandomAccount(Long userId) {
    String name = generateRandomName();
    String type = getRandomAccountType();
    return new Account(null, name, type, userId);
  }

  private String getRandomAccountType() {
    int randomNumber = random.nextInt(2);
    if (randomNumber == 1) {
      return "Standard";
    }
    return "Premium";
  }

  private String getRandomPaymentType() {
    int randomNumber = random.nextInt(2);
    if (randomNumber == 1) {
      return "Incoming";
    }
    return "Outgoing";
  }

  private Iterable<User> generateUsers(Long amountOfUsers) {
    AbstractList<User> newUsers = new ArrayList<>();
    for (long i = 0; i < amountOfUsers; i++) {
      newUsers.add(generateRandomUser());
      log.info("USER: " + i);
    }
    return userService.saveAll(newUsers);

  }

  private User generateRandomUser() {
    String firstName = generateRandomName();
    String lastName = generateRandomName();
    String email = createEmail(firstName, lastName);
    String password = "{noop}" + lastName;

    return new User(null, email, firstName, lastName, password, false);
  }


  private String createEmail(String firstName, String lastName) {
    return firstName + "." + lastName + "@gmail.com";
  }

  private String generateRandomName() {
    return generateRandomString(8L);
  }

  private String generateRandomString(Long length) {
    String string = "";

    for (int i = 0; i < length; i++) {
      int alphabetIndex = random.nextInt(25);
      string += alphabet[alphabetIndex];
    }
    return string;
  }


}
