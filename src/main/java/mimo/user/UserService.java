package mimo.user;

import java.util.AbstractList;
import org.springframework.security.core.Authentication;

public interface UserService {

  public User saveUser(User user);

  Iterable<User> saveAllUsers(AbstractList<User> users);

  public Iterable<User> getAllUsers();

  void deleteUser(Long userId);

  User getById(Long userId);

  User getUserFromAuth(Authentication auth);

  Iterable<User> saveAll(AbstractList<User> users);

  Iterable<User> getAllUsersExceptAdmins();

}
