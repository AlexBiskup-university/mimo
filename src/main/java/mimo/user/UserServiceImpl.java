package mimo.user;

import java.util.AbstractList;
import java.util.Optional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class UserServiceImpl implements UserService, UserDetailsService {


  private UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public User saveUser(User user) {

    user.setPassword(user.getPassword().replace("{noop}", ""));
    user.setPassword("{noop}" + user.getPassword());

    return userRepository.save(user);

  }

  @Override
  public Iterable<User> saveAllUsers(AbstractList<User> users) {
    return userRepository.saveAll(users);
  }

  @Override
  public Iterable<User> getAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public void deleteUser(Long userId) {
    userRepository.deleteById(userId);
  }

  @Override
  public User getById(Long userId) {
    Optional user = userRepository.findById(userId);
    if (user.isPresent()) {
      return (User) user.get();
    }
    throw new IllegalArgumentException("User not found");
  }

  @Override
  public User getUserFromAuth(Authentication auth) {
    UserDetailsImpl userDetails = (UserDetailsImpl) auth.getPrincipal();
    return userDetails.getUser();
  }

  @Override
  public Iterable<User> saveAll(AbstractList<User> users) {
    return userRepository.saveAll(users);
  }

  @Override
  public Iterable<User> getAllUsersExceptAdmins() {
    return userRepository.findAllByIsAdmin(false);
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepository.findByEmail(username);
    return new UserDetailsImpl(user);
  }
}
