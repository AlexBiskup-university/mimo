package mimo.user;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

  public User findByEmail(String email);

  Iterable<User> findAllByIsAdmin(Boolean isAdmin);

}
