package mimo.user;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/users")
public class UserController {

  private UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping
  public String getUsers(Authentication auth, Model model) {
    User user = userService.getUserFromAuth(auth);
    addPathToModel(model);
    if (user.isAdmin()) {
      model.addAttribute("users", userService.getAllUsers());
    } else {
      model.addAttribute("users", user);
    }
    model.addAttribute("user", user);
    return "users/users";
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping
  public String addUser(@RequestParam("email") String email,
      @RequestParam("first-name") String firstname, @RequestParam("last-name") String lastname,
      @RequestParam("password") String password, Model model) {
    addPathToModel(model);
    User newUser = new User(null, email, firstname, lastname, password, false);
    userService.saveUser(newUser);
    return "redirect:/users";
  }

  @PutMapping("{userId}")
  public String updateUser(Authentication auth, @PathVariable("userId") Long userId,
      @RequestParam("email") String email, @RequestParam("first-name") String firstname,
      @RequestParam("last-name") String lastname, @RequestParam("password") String password,
      Model model) {
    addPathToModel(model);
    User user = userService.getUserFromAuth(auth);
    User userToBeUpdated;
    if (user.isAdmin()) {
      userToBeUpdated = userService.getById(userId);
    } else {
      userToBeUpdated = user;
    }

    userToBeUpdated.setEmail(email);
    userToBeUpdated.setFirstname(firstname);
    userToBeUpdated.setLastname(lastname);
    userToBeUpdated.setPassword(password);

    userService.saveUser(userToBeUpdated);
    return "redirect:/users";
  }

  @DeleteMapping("{userId}")
  public String deleteUser(Authentication auth, @PathVariable("userId") Long userId, Model model) {
    addPathToModel(model);
    User user = userService.getUserFromAuth(auth);
    if (user.isAdmin()) {
      userService.deleteUser(userId);
      return "redirect:/users";
    }
    userService.deleteUser(user.getId());
    return "redirect:/logout";
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/add")
  public String getAddUserPage(Model model) {
    addPathToModel(model);
    return "users/add";
  }

  @GetMapping("{userId}/edit")
  public String getEditUserPage(Authentication auth, @PathVariable("userId") Long userId,
      Model model) {
    addPathToModel(model);
    User user = userService.getUserFromAuth(auth);
    if (user.isAdmin()) {
      model.addAttribute("user", userService.getById(userId));
    } else {
      model.addAttribute("user", user);
    }
    return "users/edit";
  }


  public void addPathToModel(Model model) {
    model.addAttribute("path", "/users");
  }
}
